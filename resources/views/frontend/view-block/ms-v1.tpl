<div class="w-full px-3 pb-6 lg:max-w-6xl lg:mx-auto">
    <div class="mb-20">
        <div class="text-3xl text-gray-900 pb-12 flex justify-center">
            <span class="ml-3 font-bold"></span>
        </div>
        <div class="grid gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-4">
            <a href=""
               class="rounded bg-white hover:shadow-lg course-item group">
                <div class="w-full thumb-box rounded-t">
                    <img src="" class="rounded-t" width="100%">
                </div>
                <div class="w-full pt-2 px-2 text-gray-800 font-bold truncate">
                </div>
                <div class="w-full flex py-2 px-2 items-center">
                    <div class="flex-1">
                        <div>
                            <span class="text-red-500 text-2xl"><small class="text-sm"></span>
                        </div>
                        <div>
                            <span class="text-gray-500 font-normal text-sm line-through"></span>
                        </div>
                    </div>
                    <div>
                        <div style="width: 137px;height: 48px;line-height:48px;background: linear-gradient(90deg, #FA1B1B 0%, #FF7946 100%);border-radius: 4px"
                             class="bg-red-500 text-white text-center">
                            立即抢购
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>