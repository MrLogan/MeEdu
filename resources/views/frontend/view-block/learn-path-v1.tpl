<div class="w-full px-3 pb-6 lg:max-w-6xl lg:mx-auto">
    <div class="mb-20">
        <div class="text-3xl text-gray-900 pb-12 flex justify-center">
            <span class="ml-3 font-bold"></span>
        </div>
        <div class="grid gap-6 grid-cols-2">
            <a href=""
               class="block p-5 rounded-lg bg-white hover:shadow-lg course-item group">
                <div class="flex">
                    <div>
                        <img src="" class="rounded-lg object-cover" width="173" height="130">
                    </div>
                    <div class="ml-5 pt-1">
                        <div class="text-xl text-gray-800"></div>
                        <div class="mt-5">
                            <span class="text-red-500 font-medium text-base"><small>￥</small></span>
                            <span class="ml-5 text-gray-400 text-xs">
                                    <span></span>
                                    <span class="mx-1">|</span>
                                    <span></span>
                                </span>
                        </div>
                        <div class="mt-5 text-xs text-gray-400">
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>